use crate::components::EntityType;
use crate::components::Position;
use crate::components::Velocity;
use specs::prelude::Join;

pub(crate) struct UpdatePosition;
impl<'a> specs::System<'a> for UpdatePosition {
    type SystemData = (
        specs::ReadStorage<'a, Velocity>,
        specs::WriteStorage<'a, Position>,
        specs::ReadStorage<'a, EntityType>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (velocity, mut position, entity_type) = data;

        for (vel, pos, _ent_type) in (&velocity, &mut position, &entity_type).join() {
            pos.0 += vel.0;
            println!("{:?}; {:?}", pos, vel);
        }
    }
}
