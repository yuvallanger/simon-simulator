use crate::components::EntityType;
use crate::components::Resources;
use crate::components::ShootDirection;
use crate::components::Velocity;
use crate::constants;
use specs::prelude::Entities;
use specs::prelude::Read;

pub(crate) struct PlayerShootsProjectile;

impl<'a> specs::System<'a> for PlayerShootsProjectile {
    type SystemData = (
        Entities<'a>,
        Read<'a, Resources>,
        Read<'a, specs::prelude::LazyUpdate>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities_read, resources_read, updater) = data;

        let player_position = &resources_read.player.position;
        let player_shoot_direction = &resources_read.player.shoot_direction;
        let player_shooting = resources_read.player.shooting;

        if !player_shooting {
            return;
        };

        let new_projectile_position = player_position.clone();
        let player_shoot_direction: ShootDirection = player_shoot_direction.clone();
        let new_projectile_velocity =
            Velocity(player_shoot_direction.to_vector(constants::PROJECTILE_SPEED));

        let new_projectile_entity = entities_read.create();

        println!(
            "New Projectile {:?} {:?} {:?}",
            &new_projectile_entity, &new_projectile_position, &new_projectile_velocity,
        );

        updater.insert(new_projectile_entity, new_projectile_position);
        updater.insert(new_projectile_entity, new_projectile_velocity);
        updater.insert(new_projectile_entity, EntityType::Bullet);
    }
}
