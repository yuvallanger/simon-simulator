// A macro to provide `println!(..)`-style syntax for `console.log` logging.
//#[cfg(target_arch = "wasm32", target_os = "unknown")]
//macro_rules! log {
//    ( $( $t:tt )* ) => {
//        web_sys::console::log_1(&format!( $( $t )* ).into());
//    }
//}
//
//#[cfg(not(target_arch = "wasm32", target_os = "unknown"))]
//macro_rules! log {
//    ( $( $t:tt )* ) => {
//        println!( $( $t )* );
//    }
//}

#[macro_export]
macro_rules! trace_value {
    ( $( $t:tt )* ) => {
        let target_string = format!(
            "{}:{}:{}",
            file!(),
            line!(),
            column!(),
        );
        let target_str = target_string.as_str();

        let message_string = format!( $( $t )* );
        let message_str = message_string.as_str();

        trace!("{}: {}", target_str, message_str);
    }
}

#[macro_export]
macro_rules! debug_value {
    ( $( $t:tt )* ) => {
        let target_string = format!(
            "{}:{}:{}",
            file!(),
            line!(),
            column!(),
        );
        let target_str = target_string.as_str();

        let message_string = format!( $( $t )* );
        let message_str = message_string.as_str();


    }
}
#[macro_export]
macro_rules! info_value {
    ( $( $t:tt )* ) => {
        let target_string = format!(
            "{}:{}:{}",
            file!(),
            line!(),
            column!(),
        );
        let target_str = target_string.as_str();

        let message_string = format!( $( $t )* );
        let message_str = message_string.as_str();

        info!("{}: {}", target_str, message_str);
    }
}

#[macro_export]
macro_rules! warn_value {
    ( $( $t:tt )* ) => {
        let target_string = format!(
            "{}:{}:{}",
            file!(),
            line!(),
            column!(),
        );
        let target_str = target_string.as_str();

        let message_string = format!( $( $t )* );
        let message_str = message_string.as_str();

        warn!("{}: {}", target_str, message_str);
    }
}
