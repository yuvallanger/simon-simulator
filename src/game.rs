//! Where we deal with the quicksilver interface.

use quicksilver::prelude::Background::Col;
use quicksilver::prelude::ButtonState;
use quicksilver::prelude::Color;
use quicksilver::prelude::Event;
use quicksilver::prelude::Key;
use quicksilver::prelude::Rectangle;
use quicksilver::prelude::Result;
use quicksilver::prelude::State;
use quicksilver::prelude::Vector;
use quicksilver::prelude::Window;
use rand::prelude::ThreadRng;
use rand::prelude::*;
use specs::prelude::Builder;
use specs::prelude::Join;
use specs::world::EntitiesRes;
use specs::Dispatcher;
use std::time::Instant;

use crate::collisions::collide_aabb;
use crate::collisions::get_position_entity_bounds;
use crate::components::FaceDirection;
use crate::components::HealthPoints;
use crate::components::HorizontalDirection;
use crate::components::Player;
use crate::components::Position;
use crate::components::Resources;
use crate::components::TicksUntilReadyToFire;
use crate::components::Velocity;
use crate::components::VerticalDirection;
use crate::components::{EntityType, KeyPosition};
use crate::constants;
use crate::constants::FIRE_KEY;
use crate::specs_systems::make_specs_dispatcher;

/// All of our game data is held within this struct.
pub(crate) struct World {
    /// `specs_world` is where we keep our entity component system.
    specs_world: specs::World,

    /// `specs_dispatch` holds all the systems of our ECS.
    specs_dispatcher: Dispatcher<'static, 'static>,

    _rng: ThreadRng,
    last_update: Instant,
    _start: Instant,
}

impl World {
    fn new() -> Self {
        let _start = Instant::now();

        let mut world = World {
            specs_world: specs::World::new(),
            specs_dispatcher: make_specs_dispatcher(),
            _rng: thread_rng(),
            last_update: _start,
            _start,
        };

        // Register resources entities and components.
        {
            let resources = Resources {
                player: Player::new(10., 10.),
                ticks_to_ready_to_fire: TicksUntilReadyToFire(0),
                ticks_since_last_enemy_spawn: 0,
                ticks_since_start: 0,
                keys_now: Default::default(),
                keys_were: Default::default(),
            };

            world.specs_world.register::<Position>();
            world.specs_world.register::<Velocity>();
            world.specs_world.register::<EntityType>();
            world.specs_world.register::<FaceDirection>();
            world.specs_world.register::<HealthPoints>();

            world.specs_world.add_resource(resources);
        }

        world
    }

    fn fire_key_handle(&mut self) {
        let mut resources = self.specs_world.write_resource::<Resources>();

        if resources.ticks_to_ready_to_fire.ready_to_fire() && resources.keys_now.fire.is_down() {
            resources.player.shooting = true;
            resources
                .ticks_to_ready_to_fire
                .reset_with(constants::FIRE_TICKS);

            return;
        }

        resources.player.shooting = false;
        resources.ticks_to_ready_to_fire.count_down();
    }

    fn _player_shoots(&mut self) {
        let _projectile_velocity: Velocity;
        let _pos: Position;
        let _entity_type: EntityType;
        {
            let mut resources = self.specs_world.write_resource::<Resources>();
            resources.player.shooting = true;
        }
        let (projectile_velocity, pos, entity_type) = {
            let resources = self.specs_world.read_resource::<Resources>();

            let projectile_velocity = Velocity(
                resources
                    .player
                    .shoot_direction
                    .to_vector(constants::PROJECTILE_SPEED),
            );
            let pos = resources.player.position.clone();
            let entity_type = EntityType::Bullet;

            (projectile_velocity, pos, entity_type)
        };
        // print!(            "New Projectile {:?} {:?} {:?}",            pos, projectile_velocity,entity_type,        );

        let _new_entity = self
            .specs_world
            .create_entity()
            .with(entity_type)
            .with(projectile_velocity)
            .with(pos)
            .build();

        // println!("{:?}", &new_entity);

        //        self.specs_world.maintain();
        {
            let mut resources = self.specs_world.write_resource::<Resources>();

            resources
                .ticks_to_ready_to_fire
                .reset_with(constants::FIRE_TICKS);
        }
    }

    fn register_input(&mut self, window: &mut Window) {
        let [left, right, up, down, fire] = [
            KeyPosition::from_is_down(window.keyboard()[Key::Left].is_down()),
            KeyPosition::from_is_down(window.keyboard()[Key::Right].is_down()),
            KeyPosition::from_is_down(window.keyboard()[Key::Up].is_down()),
            KeyPosition::from_is_down(window.keyboard()[Key::Down].is_down()),
            KeyPosition::from_is_down(window.keyboard()[FIRE_KEY].is_down()),
        ];

        {
            let mut resources = self.specs_world.write_resource::<Resources>();

            resources.keys_were = resources.keys_now.clone();
            resources.keys_now = crate::components::Keys {
                up,
                down,
                left,
                right,
                fire,
            };
        }
    }

    fn direction_keys_handle(&mut self, window: &mut Window) {
        let left_is_down = window.keyboard()[Key::Left].is_down();
        let right_is_down = window.keyboard()[Key::Right].is_down();
        let up_is_down = window.keyboard()[Key::Up].is_down();
        let down_is_down = window.keyboard()[Key::Down].is_down();

        // Set face_direction.
        {
            let mut resources = self.specs_world.write_resource::<Resources>();

            let new_face_direction = FaceDirection::from_direction_keys(
                left_is_down,
                right_is_down,
                up_is_down,
                down_is_down,
            );

            resources.player.face_direction = new_face_direction;
        }

        // Set new velocity.
        {
            let mut resources = self.specs_world.write_resource::<Resources>();

            let mut new_velocity = Velocity(Vector::new(0., 0.));

            if left_is_down {
                new_velocity.0.x -= constants::MOVEMENT_SPEED;
            };
            if right_is_down {
                new_velocity.0.x += constants::MOVEMENT_SPEED;
            };
            if up_is_down {
                new_velocity.0.y -= constants::MOVEMENT_SPEED;
            };
            if down_is_down {
                new_velocity.0.y += constants::MOVEMENT_SPEED;
            };

            resources.player.velocity = new_velocity;
        }
    }

    fn _periodic_spawn_enemy(&mut self) {
        let ticks_since_last_enemy_spawn = {
            let resources = self.specs_world.read_resource::<Resources>();
            resources.ticks_since_last_enemy_spawn
        };

        if ticks_since_last_enemy_spawn <= 0 {
            {
                self._spawn_enemy(
                    constants::SCREEN_WIDTH as f32 / 2.,
                    constants::SCREEN_HEIGHT as f32 / 5.,
                );
                //                self.specs_world.maintain();
            }
            {
                let mut resources = self.specs_world.write_resource::<Resources>();
                resources.ticks_since_last_enemy_spawn = self._rng.gen_range(
                    constants::MINIMUM_SPAWN_TICKS,
                    constants::MAXIMUM_SPAWN_TICKS,
                );
            }
        } else {
            let mut resources = self.specs_world.write_resource::<Resources>();

            resources.ticks_since_last_enemy_spawn -= 1;
        };
    }

    fn _spawn_enemy(&mut self, x: f32, y: f32) {
        self.specs_world
            .create_entity()
            .with(Position::new(x, y))
            .with(Velocity::new(
                constants::MOVEMENT_SPEED,
                constants::MOVEMENT_SPEED,
            ))
            .with(EntityType::Kharedi)
            .with(FaceDirection::new(
                HorizontalDirection::Greenwich,
                VerticalDirection::Equater,
            ))
            .with(HealthPoints(20))
            .build();
        //        self.specs_world.maintain();
    }

    fn move_player(&mut self) {
        let velocity = {
            self.specs_world
                .write_resource::<Resources>()
                .player
                .velocity
                .0
        };

        let mut resources = self.specs_world.write_resource::<Resources>();

        resources.player.position.0 += velocity;
    }

    fn delete_collided(&mut self) {
        {
            let entities = self.specs_world.entities();
            let ent_resource = self.specs_world.read_resource::<EntitiesRes>();
            let pos_storage = self.specs_world.read_storage::<Position>();
            let ent_type_storage = self.specs_world.read_storage::<EntityType>();

            for (_ent, pos, _ent_type, entity) in
                (&ent_resource, &pos_storage, &ent_type_storage, &entities).join()
            {
                //  for (ent2, pos2, ent_type_2, entity_2)
                if pos.0.x < 0.
                    || pos.0.x > constants::SCREEN_WIDTH as f32
                    || pos.0.y < 0.
                    || pos.0.y > constants::SCREEN_HEIGHT as f32
                {
                    entities.delete(entity).unwrap();
                }
            }
        }

        let mut to_delete: Vec<specs::Entity> = vec![];
        {
            let positions = self.specs_world.read_storage::<Position>();
            let entity_type = self.specs_world.read_storage::<EntityType>();

            for entity_1 in self.specs_world.entities().join() {
                if let Some(position_1) = positions.get(entity_1) {
                    for entity_2 in self.specs_world.entities().join() {
                        if let Some(position_2) = positions.get(entity_2) {
                            match (entity_type.get(entity_1), entity_type.get(entity_2)) {
                                (
                                    Some(entity_type_1 @ EntityType::Bullet),
                                    Some(entity_type_2 @ EntityType::Kharedi),
                                ) => {
                                    if collide_aabb(
                                        //into_rectangle(position_1, entity_type_1),
                                        get_position_entity_bounds(
                                            position_1.clone(),
                                            entity_type_1.clone(),
                                        ),
                                        get_position_entity_bounds(
                                            position_2.clone(),
                                            entity_type_2.clone(),
                                        ),
                                    ) {
                                        // println!(                                            "bullet {:?} kharedi {:?}",                                            position_1, position_2                                        );
                                        to_delete.push(entity_1);
                                        to_delete.push(entity_2);
                                    };
                                }
                                (_, _) => {}
                            }
                        }
                    }
                }
            }
        }
        for entity in to_delete {
            match self.specs_world.entities().delete(entity) {
                Ok(()) => println!("Deleted {:?}", entity),
                Err(e) => println!("{:?}", e),
            };
        }
        //        self.specs_world.maintain();
    }

    fn delete_outbound_projectiles(&mut self) {
        {
            let entities = self.specs_world.entities();
            //let ent_resource = self.specs_world.read_resource::<EntitiesRes>();
            let pos_storage = self.specs_world.read_storage::<Position>();
            let ent_type_storage = self.specs_world.read_storage::<EntityType>();
            for (pos, _ent_type, entity) in (&pos_storage, &ent_type_storage, &entities).join() {
                if pos.0.x < 0.
                    || pos.0.x > constants::SCREEN_WIDTH as f32
                    || pos.0.y < 0.
                    || pos.0.y > constants::SCREEN_HEIGHT as f32
                {
                    entities.delete(entity).unwrap();
                }
            }
        }

        //        self.specs_world.maintain();
    }
}

impl State for World {
    fn new() -> Result<World> {
        Ok(World::new())
    }

    fn update(&mut self, window: &mut Window) -> Result<()> {
        // Time keeping
        let now = Instant::now();
        self.last_update = now;
        //println!("{:?}", dt);

        // println!("{:?}", self.resources.player);

        // ---- Arena updates

        // self.periodic_spawn_enemy();

        // Record all input state for the duration of this tick.
        self.register_input(window);

        // Player action
        self.direction_keys_handle(window);
        self.fire_key_handle();
        self.move_player();

        // Deal with dying
        self.delete_collided();
        self.delete_outbound_projectiles();

        // Dispatch SPECS systems.
        self.specs_dispatcher.dispatch(&self.specs_world.res);
        self.specs_world.maintain();

        Ok(())
    }

    fn draw(&mut self, window: &mut Window) -> Result<()> {
        let _dt = window.current_fps();

        let resources = self.specs_world.write_resource::<Resources>();

        window.clear(Color::WHITE)?;

        let mut draw_order: Vec<(Position, EntityType)> = vec![];

        {
            //let entities = self.specs_world.entities();
            //            let entities_resource = self
            //                .specs_world
            //                .read_resource::<EntitiesRes>();
            let pos_storage = self.specs_world.read_storage::<Position>();
            let enttype_storage = self.specs_world.read_storage::<EntityType>();

            for (pos, enttype) in (&pos_storage, &enttype_storage).join() {
                draw_order.push((pos.clone(), (*enttype).clone()));
            }
        }

        draw_order.push((resources.player.position.clone(), EntityType::Yasam));

        draw_order.sort_by(|x, y| {
            if (x.0).0.y < (y.0).0.y {
                std::cmp::Ordering::Less
            } else {
                std::cmp::Ordering::Greater
            }
        });

        for (pos, ent_type) in draw_order {
            match ent_type {
                EntityType::Yasam => window.draw(
                    &Rectangle::new(
                        (pos.0.x, pos.0.y),
                        (constants::PLAYER_WIDTH, constants::PLAYER_HEIGHT),
                    ),
                    Col(Color::BLUE),
                ),
                EntityType::_Square => window.draw(
                    &Rectangle::new(
                        (pos.0.x, pos.0.y),
                        (constants::PLAYER_WIDTH, constants::PLAYER_HEIGHT),
                    ),
                    Col(Color::BLUE),
                ),
                EntityType::Kharedi => window.draw(
                    &Rectangle::new(
                        (pos.0.x, pos.0.y),
                        (constants::PLAYER_WIDTH, constants::PLAYER_HEIGHT),
                    ),
                    Col(Color::BLACK),
                ),
                EntityType::Bullet => {
                    window.draw(
                        &Rectangle::new(
                            (pos.0.x, pos.0.y),
                            (constants::PROJECTILE_WIDTH, constants::PROJECTILE_HEIGHT),
                        ),
                        Col(Color::CYAN),
                    );
                    // println!("Bullet: {}, {}", pos.0.x, pos.0.y);
                }
                _ => {}
            }
        }

        //        println!(
        //            "{:?}",
        //            (&self.player.position, &EntityType::Yasam).get_bounds()
        //        );

        Ok(())
    }
}
