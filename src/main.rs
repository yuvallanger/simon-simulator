//! YasamSim, a yasam simulator.

#[macro_use]
extern crate specs_derive;

use quicksilver::prelude::run;
use quicksilver::prelude::Settings;
use quicksilver::prelude::Vector;
use std::path;

mod collisions;
mod components;
mod constants;
mod drawing;
mod game;
mod logging;
mod specs_systems;

/*
impl DeltaTime {
    fn new(seconds: u64, nanoseconds: u32) -> Self {
        DeltaTime(std::time::Duration::new(seconds, nanoseconds))
    }
}
*/

fn main() {
    let resources_path: Option<path::PathBuf> = option_env!("CARGO_MANIFEST_DIR").map(|env_path| {
        let mut res_path: path::PathBuf = path::PathBuf::from(env_path);
        res_path.push("resources");
        res_path
    });
    println!("{:?}", resources_path);

    /*
    let mut _hello_world = HelloWorld;
    hello_world.run_now(&world.res);
    */

    run::<game::World>(
        "YasamSim",
        Vector::new(constants::SCREEN_WIDTH, constants::SCREEN_HEIGHT),
        Settings::default(),
    );
}
